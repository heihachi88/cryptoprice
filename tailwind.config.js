/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      gray: {
        100: '#F3F4F6',
        900: '#D1D5DB'
      }
    },
    extend: {},
  },
  plugins: [],
}
